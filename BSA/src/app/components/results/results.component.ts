import { Component, OnInit } from '@angular/core';
import { Result, Matches} from '../../results';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  results: Result;
  matches: Matches[];

  constructor(private resultService: DataService) { }

  ngOnInit() {
    this.loadResult();
  }

  loadResult()
  {
    this.resultService.getResults().subscribe(data => {
      this.results = data;
      this.matches = data.matches;
      console.log(this.results);

    });
  }

}
