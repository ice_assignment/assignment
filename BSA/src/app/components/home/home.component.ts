import { Component, OnInit } from '@angular/core';
import {Result} from '../../results';
import { DataService } from 'src/app/data.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  result: Result;

  constructor(private resultService: DataService) { }

  ngOnInit() {
    this.preRecord();
  }

  preRecord()
  {
    this.resultService.getHTH().subscribe(data =>{
      this.result = data;

      console.log(this.result);
    })
  }

}
