import { Component, OnInit } from '@angular/core';
import { Player, Squad } from '../../squad';
import { DataService } from 'src/app/data.service';
@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css']
})
export class PlayersComponent implements OnInit {

  player: Player;
  squad: Squad[];

  constructor(private squadService: DataService) { }

  ngOnInit() {
    this.loadPlayers();
  }

  loadPlayers()
  {
    this.squadService.getSquad().subscribe(data =>{
      this.player = data;
      this.squad = this.player.squad;

      console.log(this.player);
  });
  }
  

}
