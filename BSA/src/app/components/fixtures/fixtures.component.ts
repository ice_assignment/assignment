import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/data.service';
import { Result, Matches } from 'src/app/results';

@Component({
  selector: 'app-fixtures',
  templateUrl: './fixtures.component.html',
  styleUrls: ['./fixtures.component.css']
})
export class FixturesComponent implements OnInit {

  result: Result;
  match:  Matches[];

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.loadFixtures();
  }

  loadFixtures()
  {
    this.dataService.getUpcoming().subscribe(data =>{
      this.result = data;
      this.match = this.result.matches;
    })
  }

}
