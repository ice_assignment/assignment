import { Component, OnInit } from '@angular/core';
import { Standings, Table} from '../../standings';
import { DataService } from 'src/app/data.service';


@Component({
  selector: 'app-standings',
  templateUrl: './standings.component.html',
  styleUrls: ['./standings.component.css']
})
export class StandingsComponent implements OnInit {
  standings: Standings;
  table: Table[];

  constructor(private dataService: DataService) { }

  ngOnInit() {

    this.getStandings();
  }

  getStandings()
  {
    this.dataService.getStandings().subscribe(data =>{
      this.standings = data;

      this.table = this.standings.standings[0].table;
      console.log(this.table);
    })

  }

 
  }

