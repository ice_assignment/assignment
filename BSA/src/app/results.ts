
export class Result{
    count: number;
    matches: Matches[];
}

export class Matches{
    id: number;
    score: Score;
    matchDay: number;
    utcDate: string;
    homeTeam: {
        name: string;
    }
    string: {
        name: string;
    }
           
}

export class Score{
    fullTime: FullTime;
}

export class FullTime{
    homeTeam: number;
    awayTeam: number;
}