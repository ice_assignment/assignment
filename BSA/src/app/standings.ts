
export class Standings{
	filters: Filters;
	competition: Competition;
	season: Season; 
	standings: Standing[];
}

export class Filters{

}
export class Competition{
	id: number;
	area: Area;
	name: string;
	code: string;
	plan: string;
	lastUpdated: string;

}
export class Area{
	id: number;
	name: string;
}

export class Season{
	id: number;
	startDate: string;
	endDate: string;
	currentMatchday: number;
	winner: string;
}
export class Standing{
	stage: String;
	type: String;
	group: String;
	table: Table[];
	
}

export class Table{
	position: number;
	team: Team;
	playedGames: number;
	won: number;
	draw: number;
	loss: number;
	points: number;
	goalsFor: number;
	goalAgainst: number;
	goalDifference: number;

}

export class Team{
	id: number;
	name: string;
	crestUrl: string;
}
