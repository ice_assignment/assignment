import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Standings } from 'src/app/standings';
import { Result } from 'src/app/results';
import { Player } from 'src/app/squad';

const httpOptionsA = {
	headers: new HttpHeaders ({
    'X-Auth-Token': '256664ba6f82415c923ab2bf76f516c2'
  })
	};

@Injectable({
  providedIn: 'root'
})
export class DataService{

  constructor(private http: HttpClient) { }

  getStandings(): Observable<Standings>
  {
    let urlPrefix = "http://api.football-data.org/v2/competitions/BSA/standings";
    return this.http.get<Standings>(urlPrefix, httpOptionsA);
  }

  getUpcoming(): Observable<Result>
  {
    let urlPrefix = "http://api.football-data.org/v2/teams/6685/matches";
    return this.http.get<Result>(urlPrefix, httpOptionsA);
  }

  getSquad(): Observable<Player>
  {
    let urlPrefix = "http://api.football-data.org/v2/teams/6685";
    return this.http.get<Player>(urlPrefix, httpOptionsA);
  }

  getHTH(): Observable<Result>
  {
    let urlPrefix = "http://api.football-data.org/v2/competitions/BSA/matches?season=2018&status=FINISHED";
    return this.http.get<Result>(urlPrefix, httpOptionsA);
  }

  getResults(): Observable<Result>
  {
    let urlPrefix = "http://api.football-data.org/v2/teams/6685/matches?status=FINISHED";
    return this.http.get<Result>(urlPrefix, httpOptionsA);

  }

}




